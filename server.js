const http = require("http");
const fs = require("fs");
const uniqueid = require("uuid4");
const path = require("path");
const { url } = require("inspector");

try {
  const server = http.createServer((request, response) => {
    console.log("sever is started");
    const urlArray = request.url.split("/");

    if (request.url === "/html") {
      if (urlArray.length == 2) {
        fs.readFile("text.html", "utf-8", (err, data) => {
          if (err) {
            response.writeHead(400, { "Content-Type": "text/html" });
            response.end("<h1>check the file location</h1>");
          }
          response.writeHead(200, { "Content-Type": "text/html" });
          response.end(data);
        });
      } else {
        response.writeHead(400);
        response.end("<h1> check the url<h1/>");
      }
    } else if (request.url === "/json") {
      if (urlArray.length == 2) {
        fs.readFile("data.json", "utf-8", (err, data) => {
          if (err) {
            response.writeHead(400, { "Content-Type": "text/html" });
            response.end("<h1>check the file location</h1>");
          }
          response.writeHead(200, { "Content-Type": "application/json" });
          response.end(data);
        });
      } else {
        response.writeHead(400);
        response.end("<h1> check the url<h1/>");
      }
    } else if (request.url === "/uuid") {
      if (urlArray.length === 2) {
        let id = uniqueid();
        let dataObject = { id: id };

        response.writeHead(200, { "Content-Type": "application/json" });
        response.end(JSON.stringify(dataObject));
      } else {
        response.writeHead(400);
        response.end("<h1> check the url<h1/>");
      }
    } else if (request.url.indexOf("/status/") !== -1) {
      statusCodeObject = http.STATUS_CODES;
      requestedSatusCode = path.basename(request.url);

      if (requestedSatusCode in statusCodeObject && urlArray.length === 3) {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.end(
          JSON.stringify({
            [requestedSatusCode]: statusCodeObject[requestedSatusCode],
          })
        );
      } else {
        response.writeHead(400, { "Content-Type": "text/html" });
        response.end("<h1>check the status code</h1>");
      }
    } else if (request.url.indexOf("/delay/") !== -1) {
      time = Number(urlArray[2]);
      console.log(time);
      if (urlArray.length === 3 && !isNaN(time)) {
        let timeDelay = path.basename(request.url) * 1000;
        timeDelay = Math.abs(timeDelay);

        setTimeout(() => {
          response.writeHead(200, { "Content-Type": "text/html" });
          response.end(`<h1>Response after ${timeDelay / 1000} seconds`);
        }, timeDelay);
      } else {
        response.writeHead(400);
        response.end("<h1> check the delay time format<h1/>");
      }
    } else if (!path.basename(request.url)) {
      response.writeHead(200);
      response.end(
        "<h1>This is main server try searching with child urls</h1>"
      );
    } else {
      response.writeHead(400);
      response.end("<h1> check the url<h1/>");
    }
  });

  server.listen(2016);
} catch (err) {
  console.log(err.message);
}
